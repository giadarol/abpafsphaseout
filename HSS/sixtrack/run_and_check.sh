#!/usr/bin/bash

## set -e # does not work with explicit checks below

if [ -z "$1" ]; then
  echo "No test directory supplied." >&2
  exit 1
fi
mkdir -p "$1/HSS"
PATHTEST=$(mktemp --directory "$1/HSS/sixtrack_XXXXXX")
export PATHTEST
echo "Starting on ${HOSTNAME} at $(date) using ${PATHTEST}"

source /cvmfs/sft.cern.ch/lcg/views/LCG_94python3/x86_64-slc6-gcc8-opt/setup.sh

echo "START: $SECONDS seconds" >"$PATHTEST"/test_summary.txt

status='unknown'

cd "$PATHTEST";
git clone --no-progress https://github.com/SixTrack/SixTrack &>test_gitclone.txt
if [[ $? -eq 0 ]] ; then
   status="pass"
else
   status="fail"
   echo "git-clone FAIL" >&2
   exit 69
fi

echo "gitclone: $status, $SECONDS seconds, " >>"$PATHTEST"/test_summary.txt

cd "$PATHTEST"/SixTrack;

## AFS gets compiled in single-thread mode. Do the same for EOS (actually, everywhere)
sed -i -e '/== \/afs\//s/.*/if true; then/' -e 's/AFS/AFS and EOS/' cmake_six

./cmake_six gfortran release BUILD_TESTING &>>test_build.txt;

if [[ $? -eq 0 ]] ; then
   status="pass"
else
   status="fail"
   echo "build FAIL" >&2
   tail test_build.txt
   exit 70
fi

echo "build: $status, $SECONDS seconds, " >>"$PATHTEST"/test_summary.txt

export CTEST_OUTPUT_ON_FAILURE=1   # causes 'ctest' to spit out the exact failing command(s)
cd "$PATHTEST"/SixTrack/build/SixTrack_cmakesix_BUILD_TESTING_gfortran_release/;
ctest -L fast -E CheckBuildManual &>test_test.txt;
if [[ $? -eq 0 ]] ; then
  status="pass"
else
   status="fail"
   echo "test FAIL" >&2
   tail test_test.txt
   exit 71
fi

echo "test: $status, $SECONDS seconds, " >>"$PATHTEST"/test_summary.txt

if [[ "$status" != 'pass' ]]; then
    echo "ERROR: script ran, and gave status=$status" >&2
    exit 72
fi

if tail test_test.txt | grep -q '^100% tests passed, 0 tests failed'; then
  echo 'all OK'
  rm -rf "$PATHTEST" >& /dev/null ||:
  exit 0
fi

echo "ERROR: script did not finish running the tests" >&2
exit 75
