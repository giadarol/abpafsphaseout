#!/usr/bin/bash
start=$SECONDS
echo "START: $SECONDS seconds" >$workspace/test_summary.txt


### Definitions

workspace=`date +%Y%m%dT%H%M%S`
export appNameDef="sixtrack"
export newBuildPathDef="/afs/cern.ch/project/sixtrack/build"
export SixDeskTools="/afs/cern.ch/project/sixtrack/SixDesk_utilities/dev"






### Create workspace
$SixDeskTools/utilities/bash/set_env.sh -N $workspace








### Submit madx jobs

(cd $workspace/sixjobs;
 $SixDeskTools/utilities/bash/set_env.sh -n -l -c
 sed -i 's/hl13B1/hl10BaseB1/' sixdeskenv
 $SixDeskTools/utilities/bash/set_env.sh -s
 $SixDeskTools/utilities/bash/mad6t.sh -s
 status=`[ $? -eq 0 ] && echo "pass" || echo "fail"`
)
echo "submit madx files: $status, $SECONDS seconds" >>$workspace/test_summary.txt






### Check madx jobs

(cd $workspace/sixjobs;
 $SixDeskTools/utilities/bash/mad6t.sh -c
 while [ $? -ne 0 ];
 do
    sleep 60
    $SixDeskTools/utilities/bash/mad6t.sh -c
 done
 status=`[ $? -eq 0 ] && echo "pass" || echo "fail"`
)
echo "check madx files: $status, $SECONDS seconds" >>$workspace/test_summary.txt






### Submit SixTrack Jobs

(cd $workspace/sixjobs;
 $SixDeskTools/utilities/bash/run_six.sh -a
 status=`[ $? -eq 0 ] && echo "pass" || echo "fail"`
)
echo "run_sixtrack files: $status, $SECONDS seconds" >>$workspace/test_summary.txt






### Check Results

check_results () {
    $SixDeskTools/utilities/bash/run_six.sh -t | grep "fort.10.gz FOUND: 1200"
}

(cd $workspace/sixjobs;
 check_results
 while [ $? -ne 0 ];
 do
    sleep 60
    check_results
 done
 status=`[ $? -eq 0 ] && echo "pass" || echo "fail"`
)
echo "check sixtrack jobs: $status, $SECONDS seconds" >>$workspace/test_summary.txt




### Resubmit sixtrack jobs

(cd $workspace/sixjobs;
 $SixDeskTools/utilities/bash/run_six.sh -s -S
 check_results
 while [ $? -ne 0 ];
 do
    sleep 60
    check_results
 done
 status=`[ $? -eq 0 ] && echo "pass" || echo "fail"`
)
echo "check sixtrack jobs: $status, $SECONDS seconds" >>$workspace/test_summary.txt





### Post processing

(cd $workspace/sixjobs;
 $SixDeskTools/utilities/bash/sixdb.sh . load_dir &>test_da.txt;
 $SixDeskTools/utilities/bash/sixdb.sh hl10BaseB1.db da &>>test_da.txt;
 grep "Average: 13.86 Sigma" test_da.txt
 status=`[ $? -eq 0 ] && echo "pass" || echo "fail"`
)
echo "check postprocessing: $status, $SECONDS seconds" >>$workspace/test_summary.txt

