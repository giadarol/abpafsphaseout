#! /bin/bash

echo "Modify this file with the path you want to test."

if [ -z "$1" ]
  then
    echo "No test directory supplied."
    exit 1
fi
PATHTEST=$1/HSI/afs_phaseout_temptests_IO_test

mkdir -p $PATHTEST

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

echo -ne "Setting the environment..."
source /cvmfs/sft.cern.ch/lcg/views/LCG_95apython3/x86_64-centos7-gcc8-opt/setup.sh
echo "done."

printf "\n**** TEST ON: $PATHTEST ****\n"
python $DIR/test.py $PATHTEST/

## for comparison with afs
#PATHAFS='/afs/cern.ch/work/p/pyorbit/private/test_afs'
#printf "\n**** TEST ON: $PATHAFS ****\n"
#python test.py $PATHAFS

printf "\n**** TEST ON: /tmp/ ****\n"
python $DIR/test.py '/tmp/'
