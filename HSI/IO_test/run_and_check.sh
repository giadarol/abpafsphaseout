#! /bin/bash

if [ -z "$1" ]
  then
    echo "No test directory supplied."
    exit 1
fi

trap 'echo -ne "Error in $0:${LINENO}\nPWD=${PWD}\n\t${LINENO}\t" >&2; sed -n -e "${LINENO}p" "$0" >&2' ERR
set -e

mkdir -p "$1/HSI"
PATHTEST=$(mktemp --directory "$1/HSI/IO_test_XXXXXX")
export PATHTEST
echo "Starting on ${HOSTNAME} at $(date) using ${PATHTEST}"

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

echo -ne "Setting the environment..."
source /cvmfs/sft.cern.ch/lcg/views/LCG_95apython3/x86_64-centos7-gcc8-opt/setup.sh
echo "done."

printf "\n**** TEST ON: $PATHTEST ****\n"
python "$DIR"/test.py "$PATHTEST"/

## for comparison with afs
#PATHAFS='/afs/cern.ch/work/p/pyorbit/private/test_afs'
#printf "\n**** TEST ON: $PATHAFS ****\n"
#python test.py $PATHAFS

printf "\n**** TEST ON: /tmp/ ****\n"
python "$DIR"/test.py '/tmp/'

# TO DO - add test specific check
### check - the 'test-specific logic' is here
OK="true"
#if [ -e "${PATHTEST}"/check_HTCondor.out ]; then
#  OK="true"
#fi

### generic
if [ -n "${OK}" ]; then
  echo 'Test succeeded' >&2
  # cleanup
  rm -rf "${PATHTEST}" >& /dev/null ||:
  exit 0
fi

echo 'Test failed' >&2
echo "Test dir left in place: ${PATHTEST}"
exit 1
