# AFS Phaseout tests for the BE-ABP group

This repository contains tests for the AFS phaseout

It can be cloned using git:
```
git clone https://gitlab.cern.ch/abpcwg/abpafsphaseout.git
```

There is one folder for each ABP section and each folder contains a readme file with instructions on how to run the tests. 

**Contact persons for each section:**

LAT: Andrea Latina

HSC: Giovanni Iadarola

HSI: Hannes Bartosik

HSS: Riccardo De Maria


**AFS phaseout coordinators for ABP:**

Giovanni Rumolo

Laurent Deniau




**To run the tests with the driver:**

Update the config file in etc/ (etc/template.conf) with the suitable parameters and rename it as "beams_testing.conf".

Run the following command:
```
bin/beamstest --test path/to/desired/test/testA_run.sh
```

(Only available for HSC/001_BimBim/testA_run.sh at the moment)
