#!/usr/bin/env python3

import os,sys,pickle
import scipy.sparse as spm
import scipy.constants as cst

from BimBim.Matrix import *
from BimBim.Beams import Beams
from BimBim.Basis import Basis
from BimBim.System import System

from BimBim.Action.Damper import Damper
from BimBim.Action.Transport import Transport
from BimBim.Action.HeadOn import HeadOn
from BimBim.Action.FlatBeamBeam import FlatBeamBeam
from BimBim.Action.Impedance import Impedance
  
  
if __name__ == '__main__':
    outputDir = sys.argv[1]
    studyName = sys.argv[2]
    nSlice = int(sys.argv[3])
    nRing = int(sys.argv[4])
    sep = float(sys.argv[5])
    gain = float(sys.argv[6])
    chroma = float(sys.argv[7])
    nDim = 4
    energy = 7.0E3
    intensity = 16E11 # intesity scaling for 16 LR
    emittance = 2E-6
    sigs = 0.25E-9*cst.c
    sigp = 1.129E-4

    wakeFileName = 'EOSTest.wake';

    actionSequence = [None for k in range(200)]
    actionSequence[0] = FlatBeamBeam(beta=1.0,sepX=sep,sepY=0.0)
    actionSequence[1] = Damper(gain)
    actionSequence[2] = Impedance(wakeFileName,65.9756,71.5255,quadWake=False,intensityScaling=1.0/16.0);
    actionSequence[100] = Transport(phaseX=64.31,betaX=1.0,phaseY=59.32,betaY=1.0,qs=2.3E-3,chromaX=chroma)

    fill1 = '1 1 99 0'
    fill2 = '1 1 99 0'
    beams = Beams(fill1,fill2,energy=energy,intensity = intensity,emittance = emittance,sigs=sigs,sigp=sigp)
    
    basis = Basis(beams.getNBunchB1(),beams.getNBunchB2(),nSlice,nRing,nDim)
    
    system = System(beams,actionSequence,basis)
    
    oneTurn = system.buildOneTurnMap()
    eigvals,eigvecs = np.linalg.eig(oneTurn)
    ev = np.log(eigvals)*1j/(2*np.pi)
    myFile = open(studyName+'.pkl','wb')
    pickle.dump(ev,myFile)
    myFile.close()
