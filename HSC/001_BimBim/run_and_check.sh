#!/usr/bin/bash

# Pass working directory as argument
if [ -z "$1" ]
  then
    echo "No test directory supplied."
    exit 1
fi
trap 'echo -ne "Error in $0:${LINENO}\nPWD=${PWD}\n\t${LINENO}\t" >&2; sed -n -e "${LINENO}p" "$0" >&2' ERR
set -e

mkdir -p "$1/HSC"
PATHTEST=$(mktemp --directory "$1/HSC/001_BimBim_XXXXXX")
export PATHTEST
echo "Starting on ${HOSTNAME} at $(date) using ${PATHTEST}"

# Get dir where this test is located
TESTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# Setup and run test (testA_run.sh)
MYPATH=$(pwd)
cp -r $TESTDIR/* "$PATHTEST"/
cd "$PATHTEST"
source /cvmfs/sft.cern.ch/lcg/views/LCG_93python3/x86_64-centos7-gcc7-opt/setup.sh
export PYTHONPATH="$PYTHONPATH":"$PATHTEST"/BimBim
python3 "$PATHTEST"/BimBimTest.py "$PATHTEST" BimBimTest 11 4 10.0 0.01 15.0

# Setup and check results (testB_check.sh)
source /cvmfs/sft.cern.ch/lcg/views/LCG_93python3/x86_64-centos7-gcc7-opt/setup.sh
"$PATHTEST"/check.py "$PATHTEST"


# TO DO - add test specific check
### check - the 'test-specific logic' is here
OK="true"
#if [ -e "${PATHTEST}"/check_HTCondor.out ]; then
#  OK="true"
#fi

### generic
if [ -n "${OK}" ]; then
  echo 'Test succeeded' >&2
  # cleanup
  rm -rf "${PATHTEST}" >& /dev/null ||:
  exit 0
fi

echo 'Test failed' >&2
echo "Test dir left in place: ${PATHTEST}"
exit 1
