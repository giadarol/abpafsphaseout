#!/bin/bash


###
# Bash script for ImpedanceWake2D installation, and run on HTCondor
# The repository is cloned from GitLab
# Then install GMP, MPFR and ALGLIB and compile the code
# Then we launch the code on HTContor.
###

###
# For more infos on AFS phaseout, see G.Iadarola presentation: https://indico.cern.ch/event/812709/
# and script example: https://github.com/giadarol/afs_phaseout_ecloud_test/blob/master/test_pyecloud.sh
###

###
# 2019-05-06: script created by N. Mounet (nicolas.mounet@cern.ch)
###

# The script will stop on the first error 
trap 'echo -ne "Error in $0:${LINENO}\nPWD=${PWD}\n\t${LINENO}\t" >&2; sed -n -e "${LINENO}p" "$0" >&2' ERR
set -e

# workaround for condor_wait breakage on AFS since 8.8.6
condor_wait() { while [[ "$1" == -* ]]; do shift; done;  while ! grep Job.terminated "$1" ; do sleep 10; done }

##############################
# Path definition
##############################

# Folder in which the tests will be performed 
if [ -z "$1" ]
  then
    echo "No test directory supplied."
    exit 1
fi
mkdir -p "$1/HSC"
PATHTEST=$(mktemp --directory "$1/HSC/007_iwd2_HTC_XXXXXX")
export PATHTEST
echo "Starting on ${HOSTNAME} at $(date) using ${PATHTEST}"

COMPILATION_OUTPUT="compilation.outerr"
CONDA_OUTPUT="conda.outerr"

cd "$PATHTEST"
STARTTIME=$( date '+%s.%03N' )

#####################################################
# Clone the repository using kerberos authentication
# WARNING: the repository should be made public for the purpose of the test
#####################################################
git clone --no-progress https://:@gitlab.cern.ch:8443/IRIS/IW2D.git


##############################
# Installation and compilation
# of the C++ code
##############################

printf "\n\n**********************\nInstalling IW2D C++ code\n"

"$PATHTEST"/IW2D/Install_scripts/install_IW2D_lxplus_CERN_CC7.sh > "$PATHTEST/$COMPILATION_OUTPUT" 2>&1

printf "Done\n**********************\n"


#########################################
# Download and install miniconda (python)
#########################################

printf "\n\n**********************\nInstalling Miniconda\n"

cd "$PATHTEST"
mkdir downloads
cd downloads

wget --progress='dot:mega' https://repo.anaconda.com/miniconda/Miniconda2-latest-Linux-x86_64.sh
bash Miniconda2-latest-Linux-x86_64.sh -b -p "$PATHTEST"/miniconda2 > "$PATHTEST/$CONDA_OUTPUT" 2>&1

printf "Done\n**********************\n"


######################
# Activate miniconda #
######################
oldpython=$(which python)

printf "\n\n**********************\nActivating and installing dependencies\n"
source "${PATHTEST}/miniconda2/etc/profile.d/conda.sh"
conda activate "$PATHTEST/miniconda2"
PYTHONEXE=$(which python)
echo "Python executable: " "$PYTHONEXE"
if [[ "$oldpython" = "$PYTHONEXE" ]]; then
  echo "ERROR: failed to activate miniconda python" >&2
  exit 17
fi

# Set matplotlib backend to Agg 
# (to avoid errors if display not avaialable)
export MPLBACKEND=Agg

##############################
# Install required libraries #
##############################

pip install numpy > "$PATHTEST/$CONDA_OUTPUT" 2>&1
pip install scipy > "$PATHTEST/$CONDA_OUTPUT" 2>&1
pip install matplotlib > "$PATHTEST/$CONDA_OUTPUT" 2>&1

printf "Done\n**********************\n"

##################################
# Installation of IW2D python code
##################################

printf "\n\n**********************\nAdding the IW2D paths in conda activate script\n"

# paths for IW2D
cd "$PATHTEST"/IW2D/ImpedanceWake2D
echo $'\n# paths for ImpedanceWake2D' >> "$PATHTEST"/miniconda2/bin/activate
echo "LD_LIBRARY_PATH="`pwd`":\$LD_LIBRARY_PATH" >> "$PATHTEST"/miniconda2/bin/activate
echo "export LD_LIBRARY_PATH" >> "$PATHTEST"/miniconda2/bin/activate
echo "PATH="`pwd`":\$PATH" >> "$PATHTEST"/miniconda2/bin/activate
echo "export PATH" >> "$PATHTEST"/miniconda2/bin/activate
echo "IW2D_PATH="`pwd` >> "$PATHTEST"/miniconda2/bin/activate
echo "export IW2D_PATH" >> "$PATHTEST"/miniconda2/bin/activate

# pythonpath for general python tools
cd "$PATHTEST"/IW2D/PYTHON_codes_and_scripts/General_Python_tools
echo $'\n# Python path for General_Python_tools' >> "$PATHTEST"/miniconda2/bin/activate
echo "PYTHONPATH="`pwd`":\$PYTHONPATH" >> "$PATHTEST"/miniconda2/bin/activate
echo "export PYTHONPATH" >> "$PATHTEST"/miniconda2/bin/activate

# pythonpath for impedance library, and path for Yokoya factor file
cd "$PATHTEST"/IW2D/PYTHON_codes_and_scripts/Impedance_lib_Python
echo $'\n# paths for Impedance_lib_Python' >> "$PATHTEST"/miniconda2/bin/activate
echo "PYTHONPATH="`pwd`":\$PYTHONPATH" >> "$PATHTEST"/miniconda2/bin/activate
echo "export PYTHONPATH" >> "$PATHTEST"/miniconda2/bin/activate
echo "YOK_PATH="`pwd` >> "$PATHTEST"/miniconda2/bin/activate
echo "export YOK_PATH" >> "$PATHTEST"/miniconda2/bin/activate

source "$PATHTEST"/miniconda2/bin/activate "$PATHTEST/miniconda2"

printf "Done\n**********************\n"

##############################
# Run on HTCondor
##############################

printf "\n\n**********************\nLaunching calculations on HTCondor\n"


cat > "$PATHTEST"/run_HTCondor.py << EOF
#!$PYTHONEXE

from Impedance import imp_model_from_IW2D,impedance_wake_input,layer,Z_layer,vacuum_layer,freq_param
from string_lib import float_to_str
import numpy as np

gamma = 2.5
vac_lay = vacuum_layer(thickness=np.inf)
fpar = freq_param(fmin=1.e3,fmax=1.e8,ftypescan=0,nflog=5,fadded=[])
dire = 'test/'

for b in np.arange(1e-3,11e-3,1e-3):

    print "    b={} mm ...".format(b*1e3)
    for rho in [1e-8, 1e-7, 1e-6, 1e-5, 1e-4]:

        lay1 = Z_layer(rhoDC=rho,thickness=0.05)
        layers = [lay1,vac_lay]
        
        # round
        comment = '_round_b{}mm_rho{}muOhmm'.format(float_to_str(b*1e3),float_to_str(rho*1e6))
        iw_input = impedance_wake_input(machine='',b=[b],length=1.,layers=layers,geometry='round',
                                        comment=comment, gamma=gamma, fpar=fpar, Yokoya=[1,np.pi**2/24,np.pi**12/12,0,0])
        imp_mod,[] = imp_model_from_IW2D(iw_input,wake_calc=False,path='$IW2D_PATH',flagrm=True,
                                         lxplusbatch='launch',dire=dire)
        
        # flat
        comment = '_flat_b{}mm_rho{}muOhmm'.format(float_to_str(b*1e3),float_to_str(rho*1e6))

        iw_input = impedance_wake_input(machine='',b=[b],length=1.,layers=layers,geometry='flat',
                                        comment=comment, gamma=gamma, fpar=fpar)
        imp_mod,[] = imp_model_from_IW2D(iw_input,wake_calc=False,path='$IW2D_PATH',flagrm=True,
                                         lxplusbatch='launch',dire=dire)

    for epsb in [1.2, 1.5, 2., 3., 5.]:

        lay1 = layer(rhoDC=4e12,tau=0,epsb=epsb,mur=1,fmu=np.inf,thickness=0.05)
        layers = [lay1,vac_lay]

        # round
        comment = '_round_b{}mm_epsb{}'.format(float_to_str(b*1e3),float_to_str(epsb))
        iw_input = impedance_wake_input(machine='',b=[b],length=1.,layers=layers,geometry='round',
                                        comment=comment, gamma=gamma, fpar=fpar, Yokoya=[1,np.pi**2/24,np.pi**12/12,0,0])
        imp_mod,[] = imp_model_from_IW2D(iw_input,wake_calc=False,path='$IW2D_PATH',flagrm=True,
                                         lxplusbatch='launch',dire=dire)
        # flat
        comment = '_flat_b{}mm_epsb{}'.format(float_to_str(b*1e3),float_to_str(epsb))
        iw_input = impedance_wake_input(machine='',b=[b],length=1.,layers=layers,geometry='flat',
                                        comment=comment, gamma=gamma, fpar=fpar)
        imp_mod,[] = imp_model_from_IW2D(iw_input,wake_calc=False,path='$IW2D_PATH',flagrm=True,
                                         lxplusbatch='launch',dire=dire)

EOF

chmod +x "$PATHTEST"/run_HTCondor.py

mkdir "$PATHTEST"/HTCondor
cd "$PATHTEST"/HTCondor
"$PATHTEST"/run_HTCondor.py

ENDTIME=$( date '+%s.%03N' )
RUNTIME_init=$( echo "0$ENDTIME - 0$STARTTIME" | bc -l )


printf "HTCondor jobs submitted\n**********************\n"

############################# WAIT ########################
printf "Waiting for HTCondor jobs\n**********************\n"
# The above creates several jobs, each with own job file
#
# also, the joblogs go to some subdirectory
declare -a RUNTIMES_jobs

for joblog in "${PATHTEST}"/IW2D/ImpedanceWake2D/test/*.log; do
  jobid_tmp="${joblog##*/}"
  jobid="${jobid_tmp%.log}"
  if [[ ! -s "${joblog}" ]]; then
     echo "$(date): HTCondor joblog '${joblog}' is still empty or missing, waiting"
     sleep 60
     if [[ ! -e "${joblog}" ]]; then
        echo "ERROR: HTCondor joblog ${joblog} still not created after 60sec - job submission failed" >&2
        exit 61
     fi
     if [[ ! -s "${joblog}" ]]; then
        echo "ERROR: HTCondor joblog ${joblog} still empty after 60sec - job submission failed" >&2
        exit 62
     fi
  fi
  if condor_wait -status "$joblog"; then
    wallclocktime_tmp=$(condor_history -limit 1 -long "${jobid}" | grep RemoteWallClockTime | cut -d' ' -f 3)
    if [[ -z "${wallclocktime_tmp}" ]]; then
       ENDTIME_job=$( date '+%s.%03N' )
       wallclocktime_tmp=$( echo "0$ENDTIME_job - 0$STARTTIME" | bc -l )
       echo "WARN: HTCondor does not return RemoteWallClockTime for job ${jobid}, using wall clock" >&2
    fi
    RUNTIMES_jobs+=( "0${wallclocktime_tmp}" )
  else
    echo "ERROR: HTCondor issue with job ${jobid} logfile ${joblog}" >&2
    exit 62
  fi
done
RUNTIMES_flat="${RUNTIMES_jobs[@]}"
RUNTIME=$( echo "0${RUNTIME_init}+${RUNTIMES_flat// /+}" | bc -l)

############################# CHECK #######################

#####################################
# Check what was launched on HTCondor
#####################################

printf "\n\n**********************\nChecking calculations launched on HTCondor\n"

cat > "$PATHTEST"/check_HTCondor.py << EOF
#!$PYTHONEXE

from Impedance import imp_model_from_IW2D,impedance_wake_input,layer,Z_layer,\
            vacuum_layer,freq_param,identify_component,test_impedance_wake_comp
from string_lib import float_to_str
import numpy as np

gamma = 2.5
vac_lay = vacuum_layer(thickness=np.inf)
fpar = freq_param(fmin=1.e3,fmax=1.e8,ftypescan=0,nflog=5,fadded=[])
dire = 'test/'

def check_imp_model(imp_mod,flat=False,message=''):
    components = ['Zlong','Zxdip','Zydip','Zxquad','Zyquad']
    #if flat:
    #    components += ['Zycst']
    
    for comp in components:
        l = [len(iw.func)==26 for iw in imp_mod if test_impedance_wake_comp(iw,*identify_component(comp)[:-1])]
        assert len(l)==1 and all(l), "{}: pb with component {}".format(message,comp)


for b in np.arange(1e-3,11e-3,1e-3):

    print "    b={} mm ...".format(b*1e3)
    for rho in [1e-8, 1e-7, 1e-6, 1e-5, 1e-4]:

        lay1 = Z_layer(rhoDC=rho,thickness=0.05)
        layers = [lay1,vac_lay]
        
        # round
        comment = '_round_b{}mm_rho{}muOhmm'.format(float_to_str(b*1e3),float_to_str(rho*1e6))
        iw_input = impedance_wake_input(machine='',b=[b],length=1.,layers=layers,geometry='round',
                                        comment=comment, gamma=gamma, fpar=fpar, Yokoya=[1,np.pi**2/24,np.pi**12/12,0,0])
        imp_mod,[] = imp_model_from_IW2D(iw_input,wake_calc=False,path='$PATHTEST/IW2D/ImpedanceWake2D',flagrm=True,
                                         lxplusbatch='retrieve',dire=dire)
        check_imp_model(imp_mod,flat=False,message="b={}mm, rho={} muOhm.m, round".format(b*1e3,rho*1e6))
        
        
        # flat
        comment = '_flat_b{}mm_rho{}muOhmm'.format(float_to_str(b*1e3),float_to_str(rho*1e6))

        iw_input = impedance_wake_input(machine='',b=[b],length=1.,layers=layers,geometry='flat',
                                        comment=comment, gamma=gamma, fpar=fpar)
        imp_mod,[] = imp_model_from_IW2D(iw_input,wake_calc=False,path='$PATHTEST/IW2D/ImpedanceWake2D',flagrm=True,
                                         lxplusbatch='retrieve',dire=dire)
        check_imp_model(imp_mod,flat=True,message="b={}mm, rho={} muOhm.m, flat".format(b*1e3,rho*1e6))        

    for epsb in [1.2, 1.5, 2., 3., 5.]:

        lay1 = layer(rhoDC=4e12,tau=0,epsb=epsb,mur=1,fmu=np.inf,thickness=0.05)
        layers = [lay1,vac_lay]

        # round
        comment = '_round_b{}mm_epsb{}'.format(float_to_str(b*1e3),float_to_str(epsb))
        iw_input = impedance_wake_input(machine='',b=[b],length=1.,layers=layers,geometry='round',
                                        comment=comment, gamma=gamma, fpar=fpar, Yokoya=[1,np.pi**2/24,np.pi**12/12,0,0])
        imp_mod,[] = imp_model_from_IW2D(iw_input,wake_calc=False,path='$PATHTEST/IW2D/ImpedanceWake2D',flagrm=True,
                                         lxplusbatch='retrieve',dire=dire)
        check_imp_model(imp_mod,flat=False,message="b={}mm, epsb={}, round".format(b*1e3,epsb))        

        # flat
        comment = '_flat_b{}mm_epsb{}'.format(float_to_str(b*1e3),float_to_str(epsb))
        iw_input = impedance_wake_input(machine='',b=[b],length=1.,layers=layers,geometry='flat',
                                        comment=comment, gamma=gamma, fpar=fpar)
        imp_mod,[] = imp_model_from_IW2D(iw_input,wake_calc=False,path='$PATHTEST/IW2D/ImpedanceWake2D',flagrm=True,
                                         lxplusbatch='retrieve',dire=dire)
        check_imp_model(imp_mod,flat=True,message="b={}mm, epsb={}, flat".format(b*1e3,epsb))        

EOF

chmod +x "$PATHTEST"/check_HTCondor.py

if ! "$PATHTEST"/check_HTCondor.py -dAgg; then
    echo "ERROR: check_HTCondor.py failed" >&2
    exit 1
fi

ENDTIME_REAL=$( date '+%s.%03N' )
RUNTIME_REAL=$( echo "0${ENDTIME_REAL} - 0${STARTTIME}" | bc -l )

printf "Done\n**********************\n"

echo 'Test succeeded' >&2
echo "test_complete_test_condor_time_runtime_value_for_our_high_level_driver: ${RUNTIME}"
echo "test_runtime_real: ${RUNTIME_REAL}"
echo "test_runtimes_perjob: ${RUNTIMES_flat}"
# cleanup
rm -rf "${PATHTEST}" >& /dev/null ||:
exit 0
