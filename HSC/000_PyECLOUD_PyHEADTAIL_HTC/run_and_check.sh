#!/usr/bin/bash

# Folder in which the tests will be performed 
if [ -z "$1" ]; then
    echo "No test directory supplied." >&2
    exit 1
fi

# The script will stop on the first error 
trap 'echo -ne "Error in $0:${LINENO}\nPWD=${PWD}\n\t${LINENO}\t" >&2; sed -n -e "${LINENO}p" "$0" >&2' ERR
set -e

# workaround for condor_wait breakage on AFS since 8.8.6
condor_wait() { while [[ "$1" == -* ]]; do shift; done;  while ! grep Job.terminated "$1" ; do sleep 10; done }

mkdir -p "$1/HSC"
PATHTEST=$(mktemp --directory "$1/HSC/000_PyECLOUD_XXXXXX")
export PATHTEST
echo "Starting on ${HOSTNAME} at $(date) using ${PATHTEST}"
cd "$PATHTEST"
STARTTIME=$( date '+%s.%03N' )


###########################################
# Download and install miniconda (python) #
###########################################

mkdir downloads
cd downloads

wget --progress='dot:mega' https://repo.anaconda.com/miniconda/Miniconda2-latest-Linux-x86_64.sh
bash Miniconda2-latest-Linux-x86_64.sh -b -p "${PATHTEST}/miniconda2"

######################
# Activate miniconda #
######################
oldpython=$(which python)
source "${PATHTEST}/miniconda2/etc/profile.d/conda.sh"

conda activate "${PATHTEST}/miniconda2"

PYTHONEXE=$(which python)
echo "Python executable: " $PYTHONEXE
if [[ "$oldpython" = "$PYTHONEXE" ]]; then
  echo "ERROR: failed to activate miniconda python" >&2
  exit 17
fi

# Set matplotlib backend to Agg 
# (to avoid errors if display not avaialable)
export MPLBACKEND=Agg

##############################
# Install required libraries #
##############################

pip install numpy
pip install scipy
pip install matplotlib
pip install cython
pip install ipython
pip install h5py

#############################
# Download ABP python toots #
#############################

cd "$PATHTEST"
git clone --no-progress https://github.com/PyCOMPLETE/PyECLOUD
git clone --no-progress https://github.com/PyCOMPLETE/PyPIC
git clone --no-progress https://github.com/PyCOMPLETE/PyKLU
git clone --no-progress https://github.com/PyCOMPLETE/PyHEADTAIL
git clone --no-progress https://github.com/PyCOMPLETE/PyPARIS
git clone --no-progress https://github.com/PyCOMPLETE/NAFFlib

##########################
# Compile (f2py, cython) #
##########################

cd PyPIC
make

cd ../PyECLOUD
./setup_pyecloud

cd ../PyKLU
./install
cd ..

cd PyHEADTAIL
make
cd ..
mv PyHEADTAIL PyHEADTAIL_inst
mv PyHEADTAIL_inst/PyHEADTAIL .
    
cd NAFFlib/NAFFlib
make py2
cd ../..
mv NAFFlib NAFFlib_inst
mv NAFFlib_inst/NAFFlib .
cd ..

#################################################
# Setup and launch 10 PyECLOUD jobs on HTCondor #
#################################################

# Download example
cd "$PATHTEST"
git clone --no-progress https://github.com/giadarol/buildup_study_example

# Setup jobs
cd buildup_study_example
cd config

# FIXUP: make sure 'conda' is activated
sed -i -e '/^source .*\/miniconda2\/bin\/activate/s/$/ "$WORKFOLDER"\/miniconda2/' job.job

python config_scan.py

# Launch jobs on htcondor
cd ..
./run_htcondor

###################################################
# Setup and launch 8 instability jobs on HTCondor #
#            (parallel, 8 cores per job)          #
###################################################

# Download example
cd "$PATHTEST"
git clone --no-progress https://github.com/giadarol/instability_study_example

# Setup jobs
cd instability_study_example
cd config_PyPARIS

# FIXUP: make sure 'conda' is activated
sed -i -e '/^source .*\/miniconda2\/bin\/activate/s;$; '"$PATHTEST"/miniconda2';' sim_prototype/job.cmd

python config_scan.py

# Launch jobs on htcondor
cd ..
./run_PyPARIS_htcondor

### When the jobs are complete, plase launch test_pyecloud_checkres.sh
### to verify that the results are correct


############################################
# Exacute some scripts from the test suite #
############################################

# Execute an example from buildup test suite
cd "$PATHTEST"/PyECLOUD/testing/tests_buildup
if ! python 000_run_simulation.py --folder LHC_ArcDipReal_450GeV_sey1.70_2.5e11ppb_bl_1.00ns_stress_saver ; then
  echo 'ERROR: test failed for 000_run_simulation.py' >&2
  exit 11
fi

# Execute an example from PyEC4PyHT test suite
cd "$PATHTEST"/PyECLOUD/testing/tests_PyEC4PyHT
if ! python 009_particle_tune_shift_against_HT_multigrid.py ; then
  echo 'ERROR: test failed for 009_particle_tune_shift_against_HT_multigrid.py'
  exit 12
fi

ENDTIME=$( date '+%s.%03N' )
RUNTIME_init=$( echo "0$ENDTIME - 0$STARTTIME" | bc -l )

RUNTIME="$RUNTIME_init"
############################# WAIT ########################
echo 'HTCondor jobs submitted, waiting for completion'
for joblog in "$PATHTEST"/buildup_study_example/simulations/*/htcondor.log \
              "$PATHTEST"/instability_study_example/simulations_PyPARIS/*/htcondor.log; do
  if [[ ! -s "${joblog}" ]]; then
     echo "$(date): HTCondor joblog '${joblog}' is still empty or missing, waiting"
     sleep 60
     if [[ ! -e "${joblog}" ]]; then
        echo "ERROR: HTCondor joblog ${joblog} still not created after 60sec - job submission failed" >&2
        exit 61
     fi
     if [[ ! -s "${joblog}" ]]; then
        echo "ERROR: HTCondor joblog ${joblog} still empty after 60sec - job submission failed" >&2
        exit 62
     fi
  fi
  if ! condor_wait -status "${joblog}" ; then
    echo "$(date): HTCondor joblog ${joblog} caused an error" >&2
    exit 62
  fi
  jobid=$( sed -ne '1s/^000 (\([.0-9]\+\)) .*/\1/p' "${joblog}" )
  wallclocktime_tmp=$(condor_history -limit 1 -long "${jobid}" | grep RemoteWallClockTime | cut -d' ' -f 3)
  if [[ -z "${wallclocktime_tmp}" ]]; then
     ENDTIME_job=$( date '+%s.%03N' )
     wallclocktime_tmp=$( echo "0$ENDTIME_job - 0$STARTTIME" | bc -l )
     echo "WARN: HTCondor does not return RemoteWallClockTime for job ${jobid}, using wall clock" >&2
  fi
  RUNTIME=$( echo "0${RUNTIME}+0${wallclocktime_tmp}" | bc -l)
done



###########################################
# Check that jobs finished correctly      #
# and that output files are not corrupted #
###########################################

# Will produce an output like:
#  Check buildup jobs:
#  Successful: 10/10
#  Check instability jobs:
#  Successful: 8/8
echo "Check buildup jobs:"
cd "$PATHTEST"/buildup_study_example
if ! python check_output.py | grep 'Successful: 10/10'; then
   echo 'ERROR: buildup_study_example failed' >&2
   exit 71
fi

echo "Check instability jobs:"
cd "$PATHTEST"/instability_study_example
if ! python check_output.py | grep 'Successful: 8/8'; then
   echo 'ERROR: instability_study_example failed' >&2
   exit 72
fi


ENDTIME_REAL=$( date '+%s.%03N' )
RUNTIME_REAL=$( echo "0${ENDTIME_REAL} - 0${STARTTIME}" | bc -l )

echo 'Test succeeded' >&2
echo "test_complete_test_condor_time_runtime_value_for_our_high_level_driver: ${RUNTIME}"
echo "test_runtime_real: ${RUNTIME_REAL}"
# cleanup
rm -rf "${PATHTEST}" >& /dev/null ||:
exit 0
