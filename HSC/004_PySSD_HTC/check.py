#!/usr/bin/env python3

import os, sys
import numpy as np

pathTest = sys.argv[1]
data = np.loadtxt(os.path.join(pathTest,'output/400beta_1.5sep_500.0Ioct_-180crab/H_3.0sigma.sdiag'),delimiter=',')

if np.shape(data)!=(6,2) or np.any(np.isnan(data)) or np.any(np.isinf(data)):
    print('failed')
else:
    print('ok')
