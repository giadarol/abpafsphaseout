#!/bin/bash

source /cvmfs/sft.cern.ch/lcg/views/LCG_93python3/x86_64-centos7-gcc7-opt/setup.sh
export PYTHONPATH=$PYTHONPATH:$1:$1/PySSD
python3 PySSD_HTC.py $2 $3 $4
