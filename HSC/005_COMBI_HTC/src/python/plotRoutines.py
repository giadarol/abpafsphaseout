import matplotlib.pyplot as plt
import numpy as np
#from matplotlib.collections import LineCollection
import matplotlib.collections as col
from matplotlib import transforms
from matplotlib.colors import colorConverter


def multicoloredLine(x,y,t):
    points = np.array([x, y]).T.reshape(-1, 1, 2)
    segments = np.concatenate([points[:-1], points[1:]], axis=1)
    lc = col.LineCollection(segments, cmap=plt.get_cmap('jet'),norm=plt.Normalize(np.min(t), np.max(t)))
    lc.set_array(t)
    plt.gca().add_collection(lc)
    
def multicoloredDots(x,y,t,marker='o',s=2):
    plt.scatter(x, y, c=t, vmin=np.min(t), vmax=np.max(t), cmap=plt.get_cmap('jet'), edgecolor='none',marker=marker,s=s);
