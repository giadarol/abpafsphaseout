import numpy as np
import matplotlib.pyplot as plt
from parseOutput import *
import matplotlib as mpl

def amplitude(data):
    return np.max(data)-np.min(data);

if __name__ == '__main__':
    inputdir = '../../output/cb2/';
    outputdir = inputdir+'movie/'
    dataB1 = {};
    dataB2 = {};
    for bunch in range(36):
        dataB1[bunch] = parseBeamParameter(inputdir+"B1b"+str(bunch+1)+".bparam");
        dataB2[bunch] = parseBeamParameter(inputdir+"B2b"+str(bunch+1)+".bparam");

    analysis = 1;
    if analysis == 1:
        fig = plt.figure(0);
        for turn in range(0,len(dataB1[dataB1.keys()[0]][0])):
            print(turn)
            plt.figure(0)
            plt.plot(dataB1.keys(),[dataB1[bunch][0][turn] for bunch in dataB1.keys()],'-x',color='b');
            plt.plot(dataB2.keys(),[dataB2[bunch][0][turn] for bunch in dataB2.keys()],'-x',color='r');
            plt.title('Turn : '+str(turn));
            plt.xlabel('Bunch nb');plt.ylabel('Position');
            plt.savefig('{0:s}Osc_turn_{1:05d}.png'.format(outputdir,turn))
            plt.close()
    elif analysis == 2:
        plt.figure(0);
        amplitudes = [];
        turn = 10000;
        span = 1000;
        for bunch in data.keys():
            amplitudes.append(amplitude(data[bunch][0][turn-span:turn+span]));
        plt.plot(data.keys(),[amplitudes[k]/amplitudes[0] for k in range(len(amplitudes))],'x');
        plt.show();
