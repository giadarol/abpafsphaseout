#include <math.h>
#include "slice.h"
#include "const.h"
#include <stdio.h>

int getSliceNumber(const double& z,const int& nSlice,const double& limInf,const double& limSup) {
    //int lSlice = nSlice-1-floor(nSlice*(beam[i*NCOORD+4]-limInf)/(limSup-limInf));
    int lSlice = nSlice-1-floor(nSlice*(z-limInf)/(limSup-limInf));
    if(lSlice < 0) {
        lSlice = 0;
    } else if (lSlice > nSlice-1) {
        lSlice = nSlice -1;
    }
    return lSlice;
}

// sliceMoments [iSlice] (x)
// sliceMoments [nSlice+iSlice] (y)
// sliceMoments [2*nSlice+iSlice] (s) in [ns]
// sliceMoments [3*nSlice+iSlice] (charge)
// sliceMoments [4*nSlice+iSlice] (particle per slice)
void computeSlicesFirstOrderMoment(double* beam,int npart,double particleCharge,int nSlice,double* sliceMoments,double limInf,double limSup) {
        //initializing vectors
    for(int iSlice = 0;iSlice<nSlice;++iSlice) {
//        printf("Initializing 1st order Slices!!\n");
        sliceMoments[iSlice] = 0.0;
        sliceMoments[nSlice + iSlice] = 0.0;
        sliceMoments[2*nSlice + iSlice] = 0.0;
        sliceMoments[3*nSlice + iSlice] = 0.0;
        sliceMoments[4*nSlice + iSlice] = 0.0;
    }
    //computing slice moments
    #pragma omp parallel default(none) firstprivate(npart,nSlice,limInf,limSup,particleCharge) shared(beam,sliceMoments)
    {
        double tmpSliceM[5*nSlice];
//      printf("Initializing temp Slice variables!!\n");
        for(int i = 0;i<5*nSlice;++i) {
            tmpSliceM[i] = 0.0;
        }
        #pragma omp for schedule(guided,1000)
        for(int i = 0;i<npart;++i) {
//       printf("Summing variables per slice!!\n");
            int iSlice = getSliceNumber(beam[i*NCOORD+4],nSlice,limInf,limSup);
//        printf("Slice Number %d %E\n",iSlice,beam[i*NCOORD+4]);
//        printf("z coord: %E %E\n",beam[i*NCOORD+4],beam[i*NCOORD]);
            tmpSliceM[iSlice] += beam[i*NCOORD];
            tmpSliceM[nSlice + iSlice] += beam[i*NCOORD+2];
            tmpSliceM[2*nSlice + iSlice] += beam[i*NCOORD+4];
            tmpSliceM[3*nSlice + iSlice] += beam[i*NCOORD+6]*particleCharge;
            tmpSliceM[4*nSlice + iSlice] += 1;
        }
        //reduction
        #pragma omp critical
        {
            for(int i = 0;i<5*nSlice;++i) {
//	        printf("From temp to actual variable slice!!\n");
                sliceMoments[i] += tmpSliceM[i];
            }
        }
    }
    for(int iSlice = 0;iSlice<nSlice;++iSlice) {
 //       printf("Calculating 1st order moments\n");
        sliceMoments[iSlice] /= sliceMoments[4*nSlice + iSlice];
        sliceMoments[nSlice + iSlice] /= sliceMoments[4*nSlice + iSlice];
        sliceMoments[2*nSlice + iSlice] /= sliceMoments[4*nSlice + iSlice];
//	printf("1st order moments %E,%E,%d,%d\n",sliceMoments[4*nSlice+iSlice],sliceMoments[iSlice],iSlice,nSlice);
    }
}
//regular pizza slices
void getPizzaSliceNumber(double z,double delta,double* rms,int nSlice,int nRing,double lim,int* indices) {
    double r = sqrt(z*z/(rms[4]*rms[4])+delta*delta/(rms[5]*rms[5]));
    double theta = atan2(delta/rms[5],z/rms[4])+3.14159265359;
    indices[0] = floor(nSlice*r/lim);
    indices[1] = floor(nRing*theta/6.28318530718);
    if(indices[0]>=nSlice){
        indices[0] = nSlice-1;
    }
    if(indices[1]>=nRing){
        indices[1] = nRing-1;
    }
}

// sliceMoments [iSlice] (x)
// sliceMoments [nSlice+iSlice] (px)
// sliceMoments [2*nSlice+iSlice] (y)
// sliceMoments [3*nSlice+iSlice] (py)
// sliceMoments [4*nSlice+iSlice] (s)
// sliceMoments [5*nSlice+iSlice] (delta)
// sliceMoments [6*nSlice+iSlice] (charge)
// sliceMoments [7*nSlice+iSlice] (particle per slice)
void computePizzaSlicesFirstOrderMoment(double* beam,int npart,double particleCharge,double* rms,int nSlice,int nRing,double*** sliceMoments,double lim) {
    //initializing vectors
    for(int iSlice = 0;iSlice<nSlice;++iSlice) {
        for(int iRing = 0;iRing<nRing;++iRing) {
            sliceMoments[iSlice][iRing][0] = 0.0;
            sliceMoments[iSlice][iRing][1] = 0.0;
            sliceMoments[iSlice][iRing][2] = 0.0;
            sliceMoments[iSlice][iRing][3] = 0.0;
            sliceMoments[iSlice][iRing][4] = 0.0;
            sliceMoments[iSlice][iRing][5] = 0.0;
            sliceMoments[iSlice][iRing][6] = 0.0;
            sliceMoments[iSlice][iRing][7] = 0.0;
        }
    }
    for(int i = 0;i<npart;++i) {
        int indices[2];
        getPizzaSliceNumber(beam[i*NCOORD+4],beam[i*NCOORD+5],rms,nSlice,nRing,lim,indices);
        sliceMoments[indices[0]][indices[1]][0] += beam[i*NCOORD];
        sliceMoments[indices[0]][indices[1]][1] += beam[i*NCOORD+1];
        sliceMoments[indices[0]][indices[1]][2] += beam[i*NCOORD+2];
        sliceMoments[indices[0]][indices[1]][3] += beam[i*NCOORD+3];
        sliceMoments[indices[0]][indices[1]][4] += beam[i*NCOORD+4];
        sliceMoments[indices[0]][indices[1]][5] += beam[i*NCOORD+5];
        sliceMoments[indices[0]][indices[1]][6] += beam[i*NCOORD+6]*particleCharge;
        sliceMoments[indices[0]][indices[1]][7] += 1;
    }
    for(int iSlice = 0;iSlice<nSlice;++iSlice) {
        for(int iRing = 0;iRing<nRing;++iRing) {
            if(sliceMoments[iSlice][iRing][7]!=0){
                sliceMoments[iSlice][iRing][0] /= sliceMoments[iSlice][iRing][7];
                sliceMoments[iSlice][iRing][1] /= sliceMoments[iSlice][iRing][7];
                sliceMoments[iSlice][iRing][2] /= sliceMoments[iSlice][iRing][7];
                sliceMoments[iSlice][iRing][3] /= sliceMoments[iSlice][iRing][7];
                sliceMoments[iSlice][iRing][4] /= sliceMoments[iSlice][iRing][7];
                sliceMoments[iSlice][iRing][5] /= sliceMoments[iSlice][iRing][7];
            }
        }
    }
}

void computeSlicesSecondOrderMoment(double* beam,int npart,double particleCharge,int nSlice,double* sliceMoments2nd,double limInf,double limSup) {
        //initializing vectors
    for(int iSlice = 0;iSlice<nSlice;++iSlice) {
//        printf("Initializing !!\n");
        sliceMoments2nd[iSlice] = 0.0; // (x)
        sliceMoments2nd[nSlice + iSlice] = 0.0; // (y)
        sliceMoments2nd[2*nSlice + iSlice] = 0.0; // (s)
        sliceMoments2nd[3*nSlice + iSlice] = 0.0; // (charge)
        sliceMoments2nd[4*nSlice + iSlice] = 0.0; // (#particles)
        sliceMoments2nd[5*nSlice + iSlice] = 0.0; // (xp)
        sliceMoments2nd[6*nSlice + iSlice] = 0.0; // (yp)
        sliceMoments2nd[7*nSlice + iSlice] = 0.0; // 
        sliceMoments2nd[8*nSlice + iSlice] = 0.0; // 
        sliceMoments2nd[9*nSlice + iSlice] = 0.0; //   
        sliceMoments2nd[10*nSlice + iSlice] = 0.0; //
    }
    //computing slice moments
    // -opening parallel section (all threads execute what follows). The variable in the firstprivate will have the same value in all the threads, but are not shared. 
    // The variable in the shared are shared, i.e. no copy done in the memory.
    #pragma omp parallel default(none) firstprivate(npart,nSlice,limInf,limSup,particleCharge) shared(beam,sliceMoments2nd)
    {
        double tmpSliceM[6*nSlice];
        double tmpMean[5*nSlice];
	double tmpSliceM2[11*nSlice];
        for(int i = 0;i<6*nSlice;++i) {
            tmpSliceM[i] = 0.0;
//    tmpMean[i] = 0.0;
	}
        for(int i = 0;i<11*nSlice;++i) {
	    tmpSliceM2[i] = 0.0;
        }
        for(int i = 0;i<5*nSlice;++i) {
	    tmpMean[i] = 0.0;
        }
        // -start the loop. (the only statement acceptable after this one is a for loop) The loop is split between the threads. Guided means that the size of the chunk 
        // given to the threads decreases as the when approaching the end of the loop (optimizing the perf.)
        #pragma omp for schedule(guided,1000)
        for(int i = 0;i<npart;++i) {
            int iSlice = getSliceNumber(beam[i*NCOORD+4],nSlice,limInf,limSup);
            tmpSliceM[iSlice] += beam[i*NCOORD]; //<x>
//            printf("++ %E %E,%d\n",beam[i*NCOORD], tmpSliceM[iSlice],iSlice);
            tmpSliceM[nSlice + iSlice] += beam[i*NCOORD+2]; //<y>
            tmpSliceM[2*nSlice + iSlice] += beam[i*NCOORD+1];//<xp>
            tmpSliceM[3*nSlice + iSlice] += beam[i*NCOORD+3];//<yp>
            tmpSliceM[4*nSlice + iSlice] += beam[i*NCOORD+4];//<s>
            tmpSliceM[5*nSlice + iSlice] += 1;//nb particles
        }
//          printf("JBG  - Sum x,y,xp,yp, %d, %E,%E,%E,%E,%E\n", tmpSliceM[0],tmpSliceM[1],tmpSliceM[2],tmpSliceM[3],tmpSliceM[4]);
           for (int iSlice = 0;iSlice<nSlice;++iSlice) {
            tmpMean[iSlice] = tmpSliceM[iSlice] / tmpSliceM[5*nSlice + iSlice]; // (x)
            tmpMean[nSlice + iSlice] = tmpSliceM[nSlice + iSlice] / tmpSliceM[5*nSlice + iSlice]; // (y)
            tmpMean[2*nSlice + iSlice] = tmpSliceM[2*nSlice + iSlice] / tmpSliceM[5*nSlice + iSlice]; // (xp)
            tmpMean[3*nSlice + iSlice] = tmpSliceM[3*nSlice + iSlice] / tmpSliceM[5*nSlice + iSlice]; // (yp)
            tmpMean[4*nSlice + iSlice] = 0;
	}     
//           printf("JBG 1 - Average x,y,xp,yp, %E,%E,%E,%E\n", tmpMean[0],tmpMean[1],tmpMean[2],tmpMean[3]);
        for(int i = 0;i<npart;++i) {
            int iSlice = getSliceNumber(beam[i*NCOORD+4],nSlice,limInf,limSup);
            tmpSliceM2[iSlice] += beam[i*NCOORD]*beam[i*NCOORD]-tmpMean[iSlice]*tmpMean[iSlice]; //<x^2>
            tmpSliceM2[nSlice + iSlice] += beam[i*NCOORD+2]*beam[i*NCOORD+2]-tmpMean[nSlice + iSlice]*tmpMean[nSlice + iSlice]; //<y^2>
            tmpSliceM2[2*nSlice + iSlice] += beam[i*NCOORD+1]*beam[i*NCOORD+1]-tmpMean[3*nSlice + iSlice]*tmpMean[3*nSlice + iSlice];//<xp^2>
            tmpSliceM2[3*nSlice + iSlice] += beam[i*NCOORD+3]*beam[i*NCOORD+3]-tmpMean[4*nSlice + iSlice]*tmpMean[4*nSlice + iSlice];//<yp^2>
            tmpSliceM2[4*nSlice + iSlice] += (beam[i*NCOORD]-tmpMean[iSlice])*(beam[i*NCOORD+2]-tmpMean[nSlice + iSlice]);//<xy>
            tmpSliceM2[5*nSlice + iSlice] += (beam[i*NCOORD]-tmpMean[iSlice])*(beam[i*NCOORD+1]-tmpMean[2*nSlice + iSlice]);//<xxp>
	    tmpSliceM2[6*nSlice + iSlice] += (beam[i*NCOORD]-tmpMean[iSlice])*(beam[i*NCOORD+3]-tmpMean[3*nSlice + iSlice]);//<xyp>
	    tmpSliceM2[7*nSlice + iSlice] += (beam[i*NCOORD+2]-tmpMean[nSlice+iSlice])*(beam[i*NCOORD+1]-tmpMean[2*nSlice + iSlice]);//<yxp>
	    tmpSliceM2[8*nSlice + iSlice] += (beam[i*NCOORD+2]-tmpMean[nSlice+iSlice])*(beam[i*NCOORD+3]-tmpMean[3*nSlice + iSlice]);//<yyp>
	    tmpSliceM2[9*nSlice + iSlice] += (beam[i*NCOORD+1]-tmpMean[2*nSlice+iSlice])*(beam[i*NCOORD+3]-tmpMean[3*nSlice + iSlice]);//<xpyp>
	    tmpSliceM2[10*nSlice + iSlice] += 1;//nb particles
        }   
           {
            for(int i = 0;i<11*nSlice;++i) {
//	        printf("From temp to actual variable slice!!\n");
                sliceMoments2nd[i] += tmpSliceM2[i];
       }
    }
    }
    for(int iSlice = 0;iSlice<nSlice;++iSlice) {
        sliceMoments2nd[iSlice] /= sliceMoments2nd[10*nSlice + iSlice]; // (sigma11)
        sliceMoments2nd[nSlice + iSlice] /= sliceMoments2nd[10*nSlice + iSlice]; // (sigma33)
        sliceMoments2nd[2*nSlice + iSlice] /= sliceMoments2nd[10*nSlice + iSlice]; // (sigma22)
        sliceMoments2nd[3*nSlice + iSlice] /= sliceMoments2nd[10*nSlice + iSlice]; // (sigma44)
        sliceMoments2nd[4*nSlice + iSlice] /= sliceMoments2nd[10*nSlice + iSlice]; // (sigma13)
        sliceMoments2nd[5*nSlice + iSlice] /= sliceMoments2nd[10*nSlice + iSlice]; // (sigma12)
        sliceMoments2nd[6*nSlice + iSlice] /= sliceMoments2nd[10*nSlice + iSlice]; // (sigma14)
        sliceMoments2nd[7*nSlice + iSlice] /= sliceMoments2nd[10*nSlice + iSlice]; // (sigma23)
        sliceMoments2nd[8*nSlice + iSlice] /= sliceMoments2nd[10*nSlice + iSlice]; // (sigma34)
        sliceMoments2nd[9*nSlice + iSlice] /= sliceMoments2nd[10*nSlice + iSlice]; // (sigma24)
    }
//   printf("JBG - 2nd order Moments %E,%E,%E,%E,%E\n",sliceMoments2nd[0],sliceMoments2nd[20],sliceMoments2nd[21],sliceMoments2nd[23],sliceMoments2nd[7]);
}
