#include "parser.h"

//Dummy parser
// real parser should read the arguments to get the input file or use in.in as default and parse it

ParserOutput parse(int argc,char *argv[]) {
	ParserOutput retVal;
	retVal.collisionFile = "nocoll.in";
	retVal.fillingFileA = "fill_ref.in";
	retVal.fillingFileB = "fill_ref.in";
	retVal.outputDir = "./output";
	retVal.nturn = 11000;
	retVal.npart = 100000;
	return retVal;
}
