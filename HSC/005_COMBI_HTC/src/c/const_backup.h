#define TDEBUG 0 //display debug messages
#define RNDSEED 0 //use execution time as random seed

// HYBRID FMM
// PIC covers to   +/-(NXG)/2*DXG and +/-(NYG)/2*DYG.
// The grid should cover most or all of the particles.
// Particles outside the grid are added as discrete charges
// and may increase the computation time.

#define PI acos(-1)

#define GAUSS
