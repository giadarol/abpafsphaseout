#ifndef __ACTIONHEADER__
#define __ACTIONHEADER__

struct action {
   int actcode;
   bool needsPartner;
   double* args;
   int nargs;
};

struct actiondata {
   int actcode;
   int nargs;
   bool writeToFile;
   int turn;
   int beam;
   int partner;
   int partnerCPU;
};

void getActionStrRep(char* retVal,action act);

#endif
