#include "collimator.h"
#include "const.h"
#include <stdio.h>
#include <stdlib.h>

void collimator(double* beam, int npart, double* horLim, double* verLim,char* fileName, int turn) {
    for(int i = 0;i<npart;i++) {
        if(beam[i*NCOORD+6] != 0.0 && (beam[i*NCOORD]<horLim[0] || beam[i*NCOORD]>horLim[1] || beam[i*NCOORD+2]<verLim[0] || beam[i*NCOORD+2]>verLim[1])) {
            beam[i*NCOORD+6] = 0.0;
            FILE* file = fopen(fileName,"a+");
            fprintf(file,"%i,%i,%.20E,%.20E,%.20E,%.20E\n",i,turn,beam[i*NCOORD],beam[i*NCOORD+1],beam[i*NCOORD+2],beam[i*NCOORD+3]);
            fclose(file);
        }
    }
}
