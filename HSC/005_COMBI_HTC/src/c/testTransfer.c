#include "extern.h"
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "writer.h"

# define NXG 100
# define NYG 100
# define NGRID ((2*NXG+1)*(2*NYG+1))

//TODO understand why there are some NAN (setup of hfmm must be incomplete)

float CHG[NGRID], POTG[NGRID],EXG[NGRID],EYG[NGRID];

int main(int argc, char *argv[])
{
    unsigned int currentTime = time(NULL);
    rdmin_(&currentTime);
    unsigned int seed;
    rdmout_(&seed);
    
    int ibeam = 1;
    int ibunch = 1;
    double energy = 4000;
    double mass = 0.938272;
    double charge = 1;
    double bunchIntensity = 1E11;
    double emit = 1.5E-6;
    double betastar = 0.6;
    double bunchLength = 0.25;
    double relatvieMomentumSpread = 1.0E-4;
    int npart = 10000;
    double xfact = 0.0;
    double xkick = 0.0;
    
    initconst_(&ibeam,&ibunch,&energy,&mass,&charge);
    double particleCharge = charge >= 0.0 ? bunchIntensity/npart : -1.0*bunchIntensity/npart;
    
    double rms[NCOORD];
    rms[0] = sqrt(mass*emit*betastar);
    rms[1] = sqrt(mass*emit/betastar);
    rms[2] = sqrt(mass*emit*betastar);
    rms[3] = sqrt(mass*emit/betastar);
	rms[4] = bunchLength/4; //c*bunchLength/4
    rms[5] = relatvieMomentumSpread;
	rms[6] = 0; //intensity fluctuation
	
    double* beam = (double*)malloc(npart*NCOORD*sizeof(double));
    for(int i = 0;i<npart*NCOORD;++i) {
        beam[i] = 0;
    }
    double bparam[NPARAM];
    bfillpar_(beam,&npart,bparam,rms,&bunchIntensity,&xfact, &xkick, &seed);
    
    
    double QH = 64.3105698423;
    double QV = 59.3201154325;
    double QS = 1.90306803E-3;
    double DQH = 5.0;
    double DQV = 0.0;
    double lratio = rms[4]/rms[5];
    double betaStarH[] = {betastar,betastar};
    double betaStarV[] = {betastar,betastar};
    FILE* file = fopen("testTransfer.csv","w");
    for(int t = 0;t<10000;++t) {
        transfer_(beam,&npart,&QH, &QV,&QS,betaStarH,betaStarV,&lratio,&DQH,&DQV);
        beampar_(beam,&npart, bparam,rms,&particleCharge);
        writeBeamParameter("./","testTransfer.bparam",bparam);
        int i = 0;
        fprintf(file,"%.20E,%.20E,%.20E,%.20E,%.20E,%.20E\n",beam[i],beam[i+1],beam[i+2],beam[i+3],beam[i+4],beam[i+5]);
    }
    fclose(file);
}
