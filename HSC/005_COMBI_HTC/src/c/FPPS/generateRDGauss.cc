#include <random>
#include <stdlib.h> 
#include <fstream>
#include <iostream>
#include <sys/time.h>
#include <sys/resource.h>



using namespace std;

int main(int argc, char* argv[]){  
std::random_device rd;
  unsigned int seed(rd());
  std::default_random_engine generator(seed);
  
	double npart=1000000;
	double rmax=1.0;

  double min_r(0.0);
  double sigma(0.2);
  double min_t(0.0);
  double max_t(2*M_PI);
  std::normal_distribution<double> dist_x(0.0, sigma);
  std::normal_distribution<double> dist_y(0.0, sigma);
  std::uniform_real_distribution<double> dist_theta(0.0,M_PI); 

  std::ofstream out;
  std::string filename1="randomdist.txt";
  out.open(filename1.c_str());

double x_,y_;

  double* part = (double*) malloc(sizeof(double)*2*npart);
  
  for(int i=0; i<npart; ++i)
    {
      x_=dist_x(generator);
      y_=dist_y(generator);
      part[2*i]=x_;//x_*cos(theta);
      part[2*i+1]=y_;//x_*sin(theta);
	out<<x_<<" "<<y_<<std::endl;      
    }
	out.close();
}
