#include "const.h"
#include "defs.h"
#include "parser.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main(int argc, char *argv[])
{
    wake wake;
    parseWake("/home/xbuffat/COMBI/wakes/wakeforHDTL_Allthemachine_4TeV_B1.wake",&wake);
    for(int i=0;i<wake.size;++i) {
        printf("%f,%f,%f,%f,%f\n",wake.z[i],wake.wxDip[i],wake.wyDip[i],wake.wxQuad[i],wake.wyQuad[i]);
    }
}
