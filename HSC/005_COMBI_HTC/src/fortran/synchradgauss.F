#include <const.h>

      subroutine synchradgauss(b1,Np,
     &   equEmit,tau,betax,frac)
      implicit none
      integer Np
      double precision b1(NCOORD,Np)
      double precision :: equEmit,tau
      double precision :: betax
      double precision :: tDamping,eDamping
      double precision :: xDiff
      integer :: n
      double precision :: frac
      real :: rnd1


!    Synchrotron radiation with gaussian distribution of the 
!    energy loss per turn -> valid for a large number of photon
!    emitted per turn
!
!    the forth synchrotron integral is assumed to be 0.    
!      -> damping partitions are jx=1,jy=1,jz=2
!    The effect of quantum excitations in the longitudinal plane is neglected
!
!    equEmit the physical transverse equilibrium emittance
!    tau the transverse damping time [turn]
!    frac     Arc length (fraction of 2piR)

      tDamping = 1.0d0-1.0d0/tau
      eDamping = 1.0d0-2.0d0/tau
      xDiff = dsqrt(2.0d0*equEmit/(tau*betax))

C Random number generator is not threadsafe
C!$OMP PARALLEL DEFAULT(FIRSTPRIVATE) SHARED(b1)
C!$OMP& PRIVATE(n)
C!$OMP DO SCHEDULE(GUIDED,1000)
      do n=1,Np
        call gaussrand(rnd1)
        
        b1(2,n) = b1(2,n)*tDamping+rnd1*xDiff
        b1(4,n) = b1(4,n)*tDamping
        b1(6,n) = b1(6,n)*eDamping
      enddo
C!$OMP END DO
C!$OMP END PARALLEL

      end subroutine synchradgauss
