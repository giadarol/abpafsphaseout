      subroutine mmat(a,qx,bxa,bxb,qy,bya,byb) 

      IMPLICIT NONE

      double precision a(6,6)
      double precision qx,bxa,bxb,qy,bya,byb
c     real qx,bxa,bxb,qy,bya,byb
      double precision pi
      INTEGER I,J

      pi = 4.0*DATAN(1.0D0)

      DO J=1,6
         DO I=1,6
            A(I,J)=0.
         ENDDO
      ENDDO

      a(1,1) =  cos(2*pi*qx)*(sqrt(bxb/bxa))
      a(1,2) =  sin(2*pi*qx)*(sqrt(bxb*bxa))
      a(2,1) = -sin(2*pi*qx)/(sqrt(bxb*bxa))
      a(2,2) =  cos(2*pi*qx)*(sqrt(bxa/bxb))
      a(3,3) =  cos(2*pi*qy)*(sqrt(byb/bya))
      a(3,4) =  sin(2*pi*qy)*(sqrt(byb*bya))
      a(4,3) = -sin(2*pi*qy)/(sqrt(byb*bya))
      a(4,4) =  cos(2*pi*qy)*(sqrt(bya/byb))
      a(5,5) = 1
      a(6,6) = 1

      return
      end
