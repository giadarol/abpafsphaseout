#include <const.h>

      subroutine sbc(b1,Np,b2param,phi,nsli,alpha,
     &sliceMoments1,sliceMoments2)

      
      IMPLICIT NONE
      
      INTEGER jsli, nsli, i
      INTEGER Np
      double precision zero, half, one
      parameter(zero=0.0d0, half=0.5d0, one=1.0d0)
      double precision b1(NCOORD,Np)
      double precision phi,alpha
      double precision sphi,cphi,tphi,cphi2
      double precision salpha,calpha
      double precision s,sp
      double precision b2param(NPARAM)
      double precision h,f
      double precision h1,h1d,h1x,h1y,h1z
      double precision x1,y1,z1,det
      double precision sigz,sigzs
      double precision star
      double precision bord,bord1,border,yy
      double precision pi,gauinv
      double precision sliceMoments1(nsli*5)
      double precision sliceMoments2(nsli*11)
      double precision avpx,avpy
      dimension star(3,nsli)

      COMMON/COMBI_BEAM/IBEAM,IBUNCH,rp,ga
      INTEGER IBEAM,IBUNCH
      double precision rp, ga
      
      
      sphi=SIN(phi)
      cphi=COS(phi)
      tphi=TAN(phi)

      calpha=COS(alpha)
      salpha=SIN(alpha)
      
      avpx = 0.0 
      avpy = 0.0 
      
!      write(*,*)'CrossingAngles',sphi,cphi,tphi


!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! The idea is to paralelize in the particles loop to make it faster
! so for each strong slice we will paralelize the kick for the other beam
! We include inside here as well the Boost and anti-Boost operations
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        
!        write(*,*)'JBG beam 6',b1(1,1),Np
        
        pi=4d0*atan(1d0)  
        call wzset
        
        sigzs=0.075
        
        call stsld(star,cphi,sphi,sigzs,nsli,calpha,salpha)
        
        do jsli=nsli,1,-1 ! ( Loop over the number of slices )
      
       !sliceMoments1(jsli)=(star(3,jsli)*sphi)*calpha
       !sliceMoments1(nsli+jsli)=(star(3,jsli)*sphi)*salpha
  
!       if(ibeam.eq.1)then
!       write(23,*)sliceMoments1(jsli),sliceMoments1(nsli+jsli),         &
!     &star(3,jsli)  
!       endif
  
        do i=1,Np     ! ( Loop over the number of particles )
       
!       if(ibeam.eq.1)then
!       write(*,*)'JBG x1 y1',b1(1,i),b1(3,i),b1(2,i),b1(4,i)
!       endif
        
!       h=(b1(6,i)+one)-sqrt(((one+b1(6,i))**2-                          &
!     &b1(2,i)**2)-b1(4,i)**2)                                      
!       b1(6,i)=((b1(6,i)-(calpha*tphi)*b1(2,i))                         &
!     &-(b1(4,i)*salpha)*tphi)+h*tphi**2                               
!       b1(2,i)=(b1(2,i)-(tphi*h)*calpha)/cphi                     
!       b1(4,i)=(b1(4,i)-(tphi*h)*salpha)/cphi                     
!       h1d=sqrt(((one+b1(6,i))**2-b1(2,i)**2)-b1(4,i)**2)      
!       h1x=b1(2,i)/h1d
!       h1y=b1(4,i)/h1d
!       h1z=one-(one+b1(6,i))/h1d
!       x1=((calpha*tphi)*b1(5,i)+(one+(calpha*sphi)*h1x)*b1(1,i))       &
!     &+((b1(3,i)*salpha)*sphi)*h1x                                    
!       y1=((salpha*tphi)*b1(5,i)+(one+(salpha*sphi)*h1y)*b1(3,i))       &
!     &+((b1(1,i)*calpha)*sphi)*h1y                                    
!       b1(5,i)=b1(5,i)/cphi+h1z*((sphi*calpha)*b1(1,i)                  &
!     &+(sphi*salpha)*b1(3,i))                                         
!       b1(1,i)=x1
!       b1(3,i)=y1        
        
        
        f=-(b2param(11)*rp)/ga/dble(nsli)
!        if(i.eq.1)then
!        write(*,*)'JBG f',f
!        endif 
!        f=-((b2param(11)*(sliceMoments1(4*nsli+jsli)/Np)*rp)/ga)
        call sbmap(i,Np,b2param,cphi,cphi2,nsli,f,jsli,b1,              &
     &sliceMoments1,sliceMoments2,star)  

     
     
!        h1d=sqrt(((one+b1(6,i))**2-b1(2,i)**2)-b1(4,i)**2)      
!        h1x=b1(2,i)/h1d
!        h1y=b1(4,i)/h1d
!        h1z=one-(one+b1(6,i))/h1d
!        h1=((b1(6,i)+one)-sqrt(((one+b1(6,i))**2-                       &
!     &b1(2,i)**2)-b1(4,i)**2))*cphi**2                             
!        det=one/cphi+tphi*((h1x*calpha+h1y*salpha)-h1z*sphi)            
!        x1=(b1(1,i)*(one/cphi+(salpha*(h1y-(h1z*salpha)*sphi))*tphi)    &
!     &+((b1(3,i)*salpha)*tphi)*((h1z*calpha)*sphi-h1x))                 &
!     &-(b1(5,i)*((calpha+((h1y*calpha)*salpha)*sphi)                    &
!     &-(h1x*salpha**2)*sphi))*tphi                                      
!        y1=(((b1(1,i)*calpha)*tphi)*((h1z*salpha)*sphi-h1y)             &
!     &+b1(3,i)*(one/cphi+(calpha*(h1x-(h1z*calpha)*sphi))*tphi))        &
!     &-(b1(5,i)*(salpha-(h1y*calpha**2)*sphi                            &
!     &+((h1x*calpha)*salpha)*sphi))*tphi                                 
!        z1=(b1(5,i)*((one+(h1x*calpha)*sphi)+(h1y*salpha)*sphi)         &
!     &-((b1(1,i)*h1z)*calpha)*sphi)-((b1(3,i)*h1z)*salpha)*sphi    
!        b1(1,i)=x1/det
!        b1(3,i)=y1/det
!        b1(5,i)=z1/det
!        b1(6,i)=(b1(6,i)+(calpha*sphi)*b1(2,i))                         &
!     &+(salpha*sphi)*b1(4,i)                                       
!        b1(2,i)=(b1(2,i)+(calpha*sphi)*h1)*cphi                    
!        b1(4,i)=(b1(4,i)+(salpha*sphi)*h1)*cphi
        
!       if(ibeam.eq.1)then
!       write(*,*)'JBG x2 y2',b1(1,i),b1(3,i),b1(2,i),b1(4,i)
!       endif
        
        enddo

      enddo        
      return
      end
      
