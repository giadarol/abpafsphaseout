#include <const.h>

      subroutine bfilltestpar(b1,Np,Npreal,rms,amplX,amplY)

      IMPLICIT NONE
      integer Np,Npreal
      double precision b1(NCOORD,Np)
      double precision rms(NCOORD)
      double precision amplX(3),amplY(3),ampl0

      double precision dx,dy,tmpX,tmpY
      INTEGER I,J,JJ

      ampl0 = 0.01

      JJ = Npreal + 1
      do I = 0,nint(amplX(3))-1
        tmpX = amplX(1)+I*(amplX(2)-amplX(1))/(amplX(3)-1)
        if (tmpX.lt.ampl0) then
            tmpX = ampl0
        endif        
        do J = 0,nint(amplY(3))-1
            tmpY = amplY(1)+J*(amplY(2)-amplY(1))/(amplY(3)-1)
            if (tmpY.lt.ampl0) then
                tmpY = ampl0
            endif
            b1(1,jj) = tmpX*rms(1)
            b1(2,jj) = 0.0
            b1(3,jj) = tmpY*rms(3)
            b1(4,jj) = 0.0

            b1(5,jj) = 0.0
            b1(6,jj) = 0.0

            b1(7,jj) = 0.0;
            JJ = JJ + 1
         enddo
      enddo

      return
      end
