#!/usr/bin/bash
trap 'echo -ne "Error in $0:${LINENO}\nPWD=${PWD}\n\t${LINENO}\t" >&2; sed -n -e "${LINENO}p" "$0" >&2' ERR
set -e

# workaround for condor_wait breakage on AFS since 8.8.6
condor_wait() { while [[ "$1" == -* ]]; do shift; done;  while ! grep Job.terminated "$1" ; do sleep 10; done }

if [ -z "$1" ]; then
  echo "No test directory supplied." >&2
  exit 1
fi

mkdir -p "$1/LAT"
PATHTEST=$(mktemp --directory "$1/LAT/LAT_placet_condor_XXXXXX")
export PATHTEST
echo "Starting on ${HOSTNAME} at $(date) using ${PATHTEST}"

CONDORLOG="${PATHTEST}/testcase.log"

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

cp -r $DIR/condor/ $DIR/data_file.dat $DIR/my_placet_script.tcl "${PATHTEST}"/
sed -i -e "s;export PATHTEST=.*;export PATHTEST=${PATHTEST};" "${PATHTEST}"/condor/run.sh
# hardcode the CONDOR log info, since we will be using this to wait for the job
sed -i -e "s;log =.*;log = ${CONDORLOG};"  "${PATHTEST}"/condor/submit.sub

cd "${PATHTEST}"
# start the test - submit condor job
jobid=$(condor_submit -terse -batch-name LAT_placet_condor condor/submit.sub)
echo "Please wait for $jobid"  >&2


# wait for job to finish
joblog="${CONDORLOG}"
if [[ ! -s "${joblog}" ]]; then
     echo "$(date): HTCondor joblog '${joblog}' is still empty or missing, waiting"
     sleep 60
     if [[ ! -e "${joblog}" ]]; then
        echo "ERROR: HTCondor joblog ${joblog} still not created after 60sec - job submission failed" >&2
        exit 61
     fi
     if [[ ! -s "${joblog}" ]]; then
        echo "ERROR: HTCondor joblog ${joblog} still empty after 60sec - job submission failed" >&2
        exit 62
     fi
fi
condor_wait -status "${joblog}"

# extract runtime
last_jobid="${jobid##* }"
wallclocktime=$(condor_history -limit 1 -long "${last_jobid}" | grep RemoteWallClockTime | cut -d' ' -f 3)

### check - the 'test-specific logic' is here
OK=""
if [ -e "${PATHTEST}"/Results/output_0_1.dat ]; then
  OK="true"
fi

### generic
if [ -n "${OK}" ]; then
  echo 'Test succeeded' >&2
  echo "test_complete_test_condor_time_runtime_value_for_our_high_level_driver: ${wallclocktime}" 
  # cleanup
  rm -rf "${PATHTEST}" >& /dev/null ||:
  exit 0
else
  echo 'Test failed' >&2
  echo "Test dir left in place: ${PATHTEST}"
  exit 1
fi
