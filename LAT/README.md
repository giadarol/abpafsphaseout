# This folder contains a test for the ABP-LAT section.

# Covers:
  * PLACET: run a test simulation under HTCondor

# Usage:

```
git clone ssh://git@gitlab.cern.ch:7999/abpcwg/abpafsphaseout.git
cd abpafsphaseout/LAT/placet_condor

# edit run_test.sh to set your PATHTEST
vim run_test.sh

# edit condor/run.sh to set your PATHTEST
vim condor/run.sh

# perform the test
source ./run_test.sh

# wait for HTCondor to perform the simulation ....
condor_q

# verify if the test was successful
source ./run_check.sh
```
